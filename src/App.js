import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Projects from "./components/Projects";
import Home from "./components/Home";
import AboutMe from "./components/AboutMe";
import Resources from "./components/Resources";
import React, { useState } from "react";
import jsonProjects from "./projects.json";
import jsonResources from "./resources.json";


function App() {
  const [projects] = useState(jsonProjects);
  const [resources] = useState(jsonResources)

  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/projects" render={()=> <Projects projects={projects} />}/>
          <Route exact path="/aboutMe" component={AboutMe} />
          <Route exact path="/resources" render={()=> <Resources resources={resources} />}/>
        </Switch>
       
        <Footer/>
      </div>
    
    </BrowserRouter>
    
  );
}

export default App;
