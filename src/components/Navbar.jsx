import React from "react";
import { NavLink, Link} from "react-router-dom";
import "./../styles/Navbar.scss";

export default function Navbar() {
  return (
    <div className="Navbar">
      <div className="Navbar__links">
        <div className="Navbar__links--right">
          <NavLink exact to="/" activeClassName="active">Home</NavLink>
          <NavLink to="/projects" activeClassName="active">Proyectos</NavLink>
          <NavLink to="/aboutMe" activeClassName="active">Sobre Mí</NavLink>
          <NavLink to="/resources" activeClassName="active">Recursos</NavLink>
        </div>
      </div>
      <div className="Navbar__img">
        <Link to="/">
          <img src="/assets/logo2.png" alt="logo"></img>
        </Link>
      </div>
    </div>
  );
}
