import React from "react";

import "../styles/ProjectItem.scss";

export default function ProjectItem(props) {
  return (
    <li className="ProjectItem" key={props.title}>
      <h1 className="ProjectItem__title1">gitlab</h1>
      <div className="ProjectItem__ContainerTop1">
        <a href={props.gitlab} className="ProjectItem__ContainerTop1--year">
          (click)➤
        </a>
      </div>

      <h1 className="ProjectItem__title2">Netlify</h1>
      <div className="ProjectItem__ContainerTop2">
        <a href={props.netlify} className="ProjectItem__ContainerTop2--year3">
          (click)➤
        </a>
        <p className="ProjectItem__ContainerTop2--year4">Para ver la web.</p>
      </div>

      <div className="ProjectItem__text">
        <p className="ProjectItem__text--description">{props.description}</p>
        <p className="ProjectItem__text--skills">{props.skills}</p>
      </div>
      <div className="ProjectItem__video">
      <video controls title={props.title} src={props.video}></video>
      </div>
    </li>

  );
}
