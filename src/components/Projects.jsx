import "./../styles/Projects.scss";
import ProjectItem from "./ProjectItem";

import React from "react";

export default function Projects(props) {
  return (
    <section className="Projects">
      <ul>
        {props.projects.map(
          ({ title, description, video, skills, netlify, gitlab }) => (
            <ProjectItem
              title={title}
              description={description}
              video={video}
              skills={skills}
              key={title}
              netlify={netlify}
              gitlab={gitlab}
            />
          )
        )}
      </ul>
    </section>
  );
}
