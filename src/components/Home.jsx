import React from "react";
import "./../styles/Home.scss";

export default function Home() {
  return (
    <section className="Home">
      <div className="Home__ContainerTop">
        <p className="Home__ContainerTop--year">(2024)➤</p>
        <p className="Home__ContainerTop--year2">Me gustan los márgenes.</p>
      </div>
      <div className="Home__ContainerCenter">
        <div className="Home__ContainerCenter--img">
          <img src="/assets/menchu5.jpg" alt="imagen"></img>
        </div>
        <div className="Home__ContainerCenter--introduction">
          <h1>Hola.</h1>
          <p className="Home__ContainerCenter--introduction-p1">
            [Soy Carmen Romero, de Madrid,
          </p>
          <p className="Home__ContainerCenter--introduction-p1">
            junior front developer]
          </p>
          <p className="Home__ContainerCenter--introduction-p3">
            Quiero desarrollar proyectos útiles, bonitos y que conecten
            personas.
          </p>
        </div>
        <div className="Home__ContainerCenter--text">
          <p>
            Quiero desarrollar proyectos útiles, bonitos y que conecten
            personas.
          </p>
        </div>
      </div>

      <div className="Home__ContainerBottom">
        <p className="Home__ContainerCenter--marginRightDesktop">
          ¡Bienvenido! Tengo muchas cosas que contarte, pasa, siéntete como en
          casa.
        </p>
      </div>
    </section>
  );
}
