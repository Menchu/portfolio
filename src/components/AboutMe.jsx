import React from "react";
import "./../styles/AboutMe.scss";
import Fade from "react-reveal/Fade";

export default function AboutMe() {
  return (
    <section className="AboutMe">
      <p className="AboutMe__description1">
        Me llamo Carmen, aunque todo el mundo me llama Menchu, soy seismesina de
        nacimiento y vivo en Madrid.
      </p>

      <p className="AboutMe__description2">¿cómo estás?➤</p>
      <div className="containerSuperdesktop">
        <h1 className="AboutMe__title1-desktop">About</h1>
        <Fade left>
          <h1 className="AboutMe__title2-desktop">me</h1>
          <p className="AboutMe__thanks-desktop">
            Gracias por haber llegado hasta aquí, sé que éste no es un portfolio
            de front al uso, pero me apetecía hacer algo distinto... un poquito
            más abajo te cuento mi vida para que me conozcas mejor jaja, seré
            breve, ¡lo prometo!
          </p>
        </Fade>
        <div className="AboutMe__ContainerTop">
          <img
            className="AboutMe__ContainerTop--imgMovil"
            src="/assets/men5.jpg"
            alt="imagen"
          ></img>

          <img
            className="AboutMe__ContainerTop--imgDesktop"
            src="/assets/menchu.jpg"
            alt="imagen"
          ></img>
          <span className="AboutMe__ContainerTop--description3">
           <p>En mi anterior vida fui Fotógrafa, de ahí mi gusto por los diseños y
            el cuidado de los detalles.</p> 
          </span>
        </div>
      </div>
      <div className="AboutMe__description4">
        <div className="AboutMe__ContainerCenter">
          <p className="AboutMe__ContainerCenter--year">(2024)➤</p>
          <p className="AboutMe__ContainerCenter--year2">Un poco sobre mí.</p>
        </div>
        <p>
          Me encanta escalar, el café y soy serieadicta. Creo que la mayoría de
          mi tiempo estoy programando, intentando subir una pared más complicada
          o solucionando el mundo con los amigos...
        </p>
      </div>
      <div clssName="AboutMe__description5">
        <p className="AboutMe__description5--text">
          Soy de esas que cree que la tecnología debería servir para conectar y hacernos la vida más fácil, por eso quiero trabajar en
          proyectos útiles, bonitos y que ayuden a las personas.<br></br>Sé que
          soy un perfil junior y que me queda mucho por delante, pero lejos de
          desmotivarme creo que eso es lo que más me gusta de esta profesión,
          que siempre hay un nuevo reto y algo por aprender.<br></br> 
          Todo lo que sé hasta el momento, lo he aprendido en el Bootcamp de
          Frontend de UpgradeHub y trabajando en{" "}
          <a href="https://sygris.com/">Sygris</a> como{" "}
          Front Developer.<br></br>He aprendido
          muchísimo sobre HTML, CSS, Sass, Bem, JavaScript y un poco de Php,
          trabajamos con metodologías ágiles como Scrum y algunos
          Frameworks como Angular o Librerias como React y Redux .
          <br></br>
          Actualmente compagino mi trabajo en Sygris con algunos pequeños
          "siteProjects" por mi cuenta, que quiero enseñaros en este portfolio.
          También colaboro en la parte de desarrollo web de una asociación para
          refugiados "Abriendo Fronteras" que podéis conocer{" "}
          <a href="https://abriendofronteras.es/">aquí.</a><br></br> Si queréis saber un
          poco más sobre mi experiencia laboral, ¡os dejo mi CV para que le
          echéis un vistazo!
        </p>
      </div>

      <a
        href="/assets/CV Carmen Romero sep-2021.pdf"
        download="CV Carmen Romero López"
      >
        Descarga mi CV
      </a>
    </section>
  );
}
