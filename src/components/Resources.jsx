import React from "react";
import "./../styles/Resources.scss";

export default function Resources(props) {
  return (
    <section className="Resources">
      <h1 className="Resources__title">Re</h1>
      <div className="Resources__containerTop">
        <p className="Resources__containerTop--year">(2024)➤</p>
        <p className="Resources__containerTop--year2">
          Truquis para ir más rápido.
        </p>
      </div>
      <h1 className="Resources__title2">sources</h1>
      <p className="Resources__introduction">
        Quería aprovechar este espacio para compartir recursos, librerias y
        trucos que me han ayudado a desarrollar todos estos proyectos y otros
        futuros. Además, como no tenemos mucha documentacion en español, siempre
        viene bien sumar otro granito de arena y tener un sitio dónde buscarlos
        de manera rápida.
      </p>

      <ul>
        {props.resources.map(({ title, list = [] }) => (
          <ul className="Resources__generalList" key={title}>
            <h3>{title}</h3>
            <ul>
              {list.map(
                ({ title, importRoutes, importRoutes2, description, web }) => (
                  <li key={title}>
                    <p>{title}</p>
                    <p>{importRoutes}</p>
                    <p>{importRoutes2}</p>
                    <p>{description}</p>
                    <p>{web}</p>
                  </li>
                )
              )}
            </ul>
          </ul>
        ))}
      </ul>
    </section>
  );
}
