import React from "react";
import "./../styles/Footer.scss";

export default function Footer() {
  return (
    <div className="Footer">
      <div className="Footer__anchor">
        <a href="https://www.linkedin.com/in/carmen-romero-l%C3%B3pez-32359951/">
          [linkedin].
        </a>
        <a href="https://gitlab.com/Menchu">[gitlab].</a>
      </div>
      <div className="Footer__introduction"></div>
    </div>
  );
}
